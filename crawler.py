import sqlite3
import requests
from bs4 import BeautifulSoup
import database as db
import networkx as nx


class Crawler:

    def parsed_html(self, url):
        code = requests.get(url)
        status = (code.status_code)
        plain = code.text
        soup = BeautifulSoup(plain, "html.parser")
        return status, plain, soup

    def get_urls(self, s, Web_Urls, home_url):
        for link in s.findAll('a'):
            l = link.get('href')
            if l is not None and l[0] == '/' and l != '/':
                if home_url+l not in Web_Urls:
                    Web_Urls.append(home_url+l)
        return Web_Urls

    def web(self, Web_Urls, mydb, table_name):
        i = 0
        j = 0
        while j < len(Web_Urls):
            url = Web_Urls[j]
            status, plain, soup = self.parsed_html(url)
            if status == 200:
                WebUrls = self.get_urls(soup, Web_Urls, Web_Urls[0])
                if db.check_url_in_db(mydb, table_name, Web_Urls[j]):
                    db.insert_webpage_in_db(
                        mydb, table_name, Web_Urls[j], plain, status)
            j += 1


    def link_map(self, Web_Urls, mydb, table_name):
        j = 0
        while j < len(Web_Urls):
            # print(Web_Urls)
            url = Web_Urls[j]
            #print(j, "  ", url)
            code = requests.get(url)
            s = BeautifulSoup(code.text, "html.parser")
            links = self.get_urls(s, [url], Web_Urls[0])
            for link in links:
                db.insert_in_url_map(mydb, table_name, url, link)
            for link in links:
                if link not in Web_Urls:
                    Web_Urls.append(link)
            j += 1

    def add_edges(self, mydb, table_name):
        edges = []
        results = db.get_edges(mydb, table_name)
        for result in results:
            edges.append((result[0], result[1]))
        return edges

    def create_network(self, mydb, table_name, source, destination):
        G = nx.DiGraph()
        #G.add_nodes_from(self.add_nodes(mydb, table_name))
        G.add_edges_from(self.add_edges(mydb, table_name))
        path = nx.shortest_path(G, source, destination)
        distance = nx.shortest_path_length(G, source, destination)
        return path, distance
