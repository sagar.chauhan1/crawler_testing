import unittest
import crawler as cr
import database as db
import sqlite3


class Test_crawler(unittest.TestCase):
    """docstring for Test_crawler"""

    def setUp(self):
        print("setting up testing enviornment")
        #self.mydb1 = db.connect_db("test.db")
        self.mydb = db.connect_db("test4.db")
        self.url = "http://localhost:8001"
        self.table_name = "html_content"
        self.urlmap_table = "url_map"
        db.create_table(self.mydb, self.table_name)
        db.create_url_map_table(self.mydb, self.urlmap_table)
        return self

    def test_parsed_html(self):
        obj = cr.Crawler()
        status, plain, self.soup = obj.parsed_html(self.url)
        expected_output = 200
        assert expected_output == status

    def test_get_urls(self):
    	obj = cr.Crawler()
    	status, plain, soup = obj.parsed_html(self.url)
    	Web_Urls = [self.url]
    	actual_output = obj.get_urls(soup, Web_Urls, Web_Urls[0])
    	expected_output = ['http://localhost:8001', 'http://localhost:8001/1.html']
    	assert actual_output == expected_output
    	
        
    def test_crawl_data(self):
        obj = cr.Crawler()
        obj.web([self.url], self.mydb, self.table_name)
        cursor = self.mydb.cursor()
        cursor.execute("SELECT * from "+self.table_name)
        results = cursor.fetchall()
        actual_output = []
        expected_output = ['http://localhost:8001', 'http://localhost:8001/1.html', 'http://localhost:8001/2.html', 'http://localhost:8001/4.html', 'http://localhost:8001/6.html',
                           'http://localhost:8001/8.html', 'http://localhost:8001/10.html', 'http://localhost:8001/3.html', 'http://localhost:8001/9.html', 'http://localhost:8001/7.html', 'http://localhost:8001/5.html']
        for result in results:
            actual_output.append(result[1])
        assert actual_output == expected_output

    def test_shortest_path(self):
        obj = cr.Crawler()
        obj.link_map([self.url], self.mydb, self.urlmap_table)
        path, distance = obj.create_network(
            self.mydb, self.urlmap_table, 'http://localhost:8001', 'http://localhost:8001/5.html')
        assert (path, distance) == (['http://localhost:8001', 'http://localhost:8001/1.html',
                                     'http://localhost:8001/4.html', 'http://localhost:8001/5.html'], 3)

    def tearDown(self):
        cursor = self.mydb.cursor()
        cursor.execute("DROP TABLE "+self.table_name)
        cursor.execute("DROP TABLE "+self.urlmap_table)


if __name__ == '__main__':
    unittest.main()
