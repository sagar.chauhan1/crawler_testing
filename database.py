import sqlite3
import os


def insert_webpage_in_db(mydb, table_name, link, content, status):
    cursor = mydb.cursor()
    cursor.execute('''INSERT INTO '''+table_name+''' (status, url, content) VALUES
         (?,?,?)''', (status, link, content))
    mydb.commit()


def connect_db(db_name):
    mydb = sqlite3.connect(db_name)
    return mydb


def create_table(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS '''+table_name+'''
     (status INTEGER, url VARCHAR PRIMARY KEY, content VARCHAR );''')


def check_url_in_db(mydb, table_name, url):
    cursor = mydb.cursor()
    cursor.execute(
        "SELECT count(url) from "+table_name+" WHERE url = '"+url+"';")
    x = cursor.fetchall()[0][0]
    if x == 0:
        return True
    else:
        return False


def create_url_map_table(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS '''+table_name+'''
     ( source VARCHAR, destination VARCHAR);''')


def insert_in_url_map(mydb, table_name, source, destination):
    cursor = mydb.cursor()
    cursor.execute('''INSERT INTO '''+table_name+''' (source, destination) VALUES
         (?,?)''', (source, destination))
    mydb.commit()

def get_edges(mydb, table_name):
    cursor = mydb.cursor()
    cursor.execute("SELECT * from "+table_name)
    results = cursor.fetchall()
    return results


